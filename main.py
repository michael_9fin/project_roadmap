import os
from dotenv import load_dotenv

from logic.draw_project_roadmap import draw_roadmap


load_dotenv()

# Project milestones with status of work
milestones = [
    {"milestone": "Requirements gathering", "status": "complete"},
    {"milestone": "Begin awesome project", "status": "complete"},
    {"milestone": "Boil water in kettle", "status": "complete"},
    {"milestone": "Put tea bag in cup", "status": "diss-staging"},
    {
        "milestone": "Pour in milk before water - joke - pour water into cup",
        "status": "current",
    },
    {"milestone": "Add milk and sugar according to preferences.", "status": "todo"},
    {"milestone": "Stir.", "status": "todo"},
    {"milestone": "Drink tea", "status": "todo"},
]


draw_roadmap(
    milestones=milestones,
    file_name=f"{os.getenv('FILE_NAME')}.pdf",
    project_title=os.getenv("PROJECT_TITLE"),
)
