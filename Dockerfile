# Pull an official base image
FROM python:3.11-slim-bullseye

WORKDIR /code
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt


# Create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home
USER appuser

# Install application into container
COPY ./ ./

# Grant appuser ownership of the data folder
USER root
RUN chown -R appuser /home/data/


# Switch back to appuser
USER appuser

CMD ["python", "-u", "main.py"]