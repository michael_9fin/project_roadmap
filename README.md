<h1 style="text-decoration:underline; text-align:center; color:#3A78FF">Project RoadMap Visualisation</h1>
Simple code to generate a project roadmap visualisation to help communicate project milestones and current progress.

A pdf is generated with a visualisation which can be shared or simply take a screenshot of the image.

[ReportLab](https://www.reportlab.com/) is an [Open Source Python library](https://pypi.org/project/reportlab/) for 
generating PDFs and graphics. A copy of the reportlab user manual is saved in the docs folder.


<h2 style="text-decoration:underline; text-align:left; color:#3A78FF">Set Up</h2>

1. Copy the `.env_example` file and rename it `.env`.
2. Fill in the project title, and the name of the file.
3. In `main.py` fill in the project milestones and statuses.

![project milestones](images/project_milestones.png)
<h2 style="text-decoration:underline; text-align:left; color:#3A78FF">Docker</h2>

1. In the project root terminal type `docker compose up`.
2. This will generate and save a pdf file to the `data` folder.

<p align="center" width="100%">
<img src="images/pdf_saved.png">
</p>
<p align="center" width="100%">
<img src="images/project_roadmap.png"  width="900" height="600">
</p>

<h2 style="text-decoration:underline; text-align:left; color:#3A78FF">Local Development</h2>
If you would like to alter/extend the code, in the project root terminal type:
1. `make venv` to create a virtual environment.
2. `source venv/bin/activate` to activate the virtual environment.
3. `make deps` to install the project dependencies.
4. `make lint` to run black, followed by flake8 and ruff for linting.

<h2 style="text-decoration:underline; text-align:left; color:#3A78FF">Developers</h2>
Michael Brown - michael@9fin.com