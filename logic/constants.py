colours = {
    "9fin_blue": "#007CFF",
    "red": "#FF0000",
    "dark_blue": "#0a2f5b",
    "green": "#2E8B57",
    "orange": "#FF7F50",
}
status_colours = {
    "complete": colours["green"],
    "diss-staging": colours["dark_blue"],
    "current": colours["orange"],
    "todo": colours["red"],
}
