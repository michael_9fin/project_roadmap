import os

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.colors import HexColor
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4, landscape

from config import BASE_PATH
from logic.constants import colours, status_colours
from logic.generators import infinite_sequence
from logic.pdf_logic import (
    draw_centred_rectangle,
    draw_text_label,
    draw_vertical_dotted_line,
    draw_legend,
)


def draw_roadmap(
    milestones: list[dict], file_name: str | None, project_title: str | None
) -> None:
    """Save a PDF file with the project roadmap visualisation.

    Args:
        milestones (list[dict]): key, value pairs of project description and
        statuses.
        file_name (str): Location and name of file to save as.
        project_title (str): Title of the project.

    Returns: None
    """

    # A4 paper canvas dimensions
    a4_height, a4_width = A4
    rect_height = 3 * cm

    if not file_name:
        file_name = "pdf_report"

    if not project_title:
        project_title = "Project Roadmap"

    canvas = Canvas(
        filename=os.path.join(BASE_PATH, "data", file_name), pagesize=landscape(A4)
    )

    # Title
    canvas.setFont("Helvetica", 26)
    canvas.drawCentredString(
        x=a4_width / 2,
        y=a4_height * 0.90,
        text=project_title,
    )

    # Legend
    canvas.setFont("Helvetica", 14)
    draw_legend(canvas=canvas)

    # Draw background rectangle
    rect_x, rect_y, rect_width = draw_centred_rectangle(
        canvas=canvas,
        page_height=a4_height,
        page_width=a4_width,
        x_gap=4 * cm,
        y_gap=13 * cm,
        rect_height=rect_height,
        colour=colours["9fin_blue"],
    )

    # Location of first circle milestones in vertical middle of rectangle
    circle_x = rect_x + 1 * cm
    circle_y = rect_y + rect_height / 2
    circle_radius = 0.3 * cm

    canvas.setFillColor(HexColor("#A6A6A6"), alpha=1)
    canvas.setStrokeColorRGB(0 / 255, 0 / 255, 0 / 255, alpha=1)
    canvas.roundRect(
        x=rect_x,
        y=circle_y - (circle_radius / 2),
        width=rect_width,
        height=0.25 * cm,
        radius=4,
        stroke=1,
        fill=1,
    )

    canvas.translate(circle_x, circle_y - (5 * cm))

    # A4: 210 mm width, 297 mm height
    conversion = 29 / 792
    milestone_spacer = rect_width / len(milestones) * conversion

    # Alternate up/down dotted lines to text boxes
    up_down_gen = infinite_sequence(nums=[-1, 1])

    # Alternate short and long lines to text boxes to make reading easier
    four_four_six_six_gen = infinite_sequence([4, 4, 6, 6])

    for index, row in enumerate(milestones):
        # Milestone circle colour
        canvas.setFillColor(HexColor(status_colours[row["status"]]))

        # Alternating up/down vertical lines
        up_or_down = next(up_down_gen)

        # Long, Long, Short, Short lines leading to text boxes.
        y_end = next(four_four_six_six_gen)

        # next location of circle x centre
        spacer = index * milestone_spacer

        draw_vertical_dotted_line(
            canvas, spacer * cm, 5 * cm, 5 * cm + (up_or_down * y_end * cm)
        )

        canvas.circle(spacer * cm, 5 * cm, circle_radius, stroke=1, fill=1)

        # Milestone description
        draw_text_label(
            canvas=canvas,
            x_coord=(spacer - 2) * cm,
            y_coord=5 * cm + (up_or_down * y_end * cm),
            text=row["milestone"],
        )

        canvas.setFillColorRGB(0 / 255, 0 / 255, 0 / 255, alpha=1)

    canvas.showPage()
    canvas.save()

    print(f"Document saved to: {file_name}")
