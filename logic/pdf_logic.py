from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.units import cm
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Paragraph

from logic.constants import status_colours


def draw_centred_rectangle(
    canvas, page_height, page_width, x_gap, y_gap, rect_height, colour
):
    """Draw a rectangle in the centre of the canvas at a given height.

    Args:
        canvas (Canvas): Interface to the PDF file format.
        page_height (float): Height of the page.
        page_width (float): Width of the page.
        x_gap (float): Combined gap on left and right of rectangle.
        y_gap (float): Gap above rectangle.
        rect_height (float): Height of the rectangle.
        colour (str): Hex colour to fill rectangle.

    Returns:
        rect_measurements (tuple [float]): rect_x, rect_y, rect_width
    """

    canvas.setStrokeColor(colour)
    canvas.setFillColor(colour)

    rect_width = page_width - x_gap
    rect_x = (page_width - rect_width) / 2
    rect_y = page_height - y_gap
    canvas.rect(x=rect_x, y=rect_y, width=rect_width, height=rect_height, fill=1)

    return rect_x, rect_y, rect_width


def draw_text_label(canvas: Canvas, x_coord: float, y_coord: float, text: str) -> None:
    """Draw a rectangle with text inside.

    Args:
        canvas (Canvas): Interface to the PDF file format.
        x_coord (float): x-coordinate
        y_coord (float): y-coordinate
        text (str): Text to write inside rectangle.

    Returns: None
    """

    p = ParagraphStyle("orden")
    p.max_width = 30
    p.textColor = "black"
    p.borderColor = "black"
    p.borderWidth = 1
    p.alignment = TA_CENTER
    p.fontSize = 10
    p.leading = 20
    p.borderPadding = 10
    p.backColor = colors.white
    para = Paragraph(text, p)
    canvas.setStrokeColorRGB(0.5, 0.5, 0.5)
    canvas.setFillColorRGB(0.5, 0.5, 0.5)

    para.wrapOn(canvas, 130, 100)
    para.drawOn(canvas, x_coord, y_coord)


def draw_dotted_line(canvas, x: int, y: int, length: int):
    """Draw a horizontal dotted line.

    Args:
        canvas (Canvas): Interface to the PDF file format.
        x (int): x-coordinate.
        y (int): y-coordinate.
        length (int): lenght of line

    Returns: None
    """

    canvas.setDash(1, 3)  # set the pattern of the line
    canvas.line(x, y, x + length, y)  # draw the line


def draw_legend(
    canvas,
    circle_x: float = 0.5,
    circle_radius: float = 0.1,
    label_string_x=100,
    legend_y: float = 1.8,
) -> None:
    """Draw legend on canvas.

    Args:
        canvas (Canvas): Interface to the PDF file format.
        circle_x: x-coordinate of legend circle.
        circle_radius (float): Radius of circle
        label_string_x: x-coordinate of legend label.
        legend_y: y-coordinate of legend label.

    Returns: None
    """

    max_label_length = max(
        [canvas.stringWidth(status) for status in status_colours.keys()]
    )

    for status, status_colour in status_colours.items():
        canvas.setFillColor(status_colour)
        canvas.circle(
            x_cen=circle_x * cm,
            y_cen=legend_y * cm,
            r=circle_radius * cm,
            stroke=1,
            fill=1,
        )

        text_length = canvas.stringWidth(status)

        canvas.drawString(
            x=label_string_x - text_length - (max_label_length - text_length),
            y=(legend_y - circle_radius) * cm,
            text=status,
        )
        legend_y -= 0.5


def draw_vertical_dotted_line(canvas, x, y_start, y_end):
    """Draw a vertical dotted line.

    Args:
        canvas (Canvas): Interface to the PDF file format.
        x (float): x-coordinate where line will be drawn.
        y_start (float): y-coordinate where line will begin.
        y_end (float): y-coordinate where line will end.

    Returns: None
    """

    canvas.setDash(1, 4)  # set the dash pattern to 1 pixel on, 4 pixels off
    canvas.setStrokeColorRGB(0, 0, 0)  # set the stroke color to black
    canvas.line(x, y_start, x, y_end)  # draw the line
    canvas.setDash(1, 0)
