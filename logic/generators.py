from typing import Generator


def infinite_sequence(nums: list[int]) -> Generator:
    """Iterate over an infinite sequence.

    Args:
        nums (list[int]): The list to interate over.

    Returns:
        integer (int): An integer value.
    """

    index = 0
    reset = len(nums)

    while True:
        yield nums[index]
        index += 1
        if index >= reset:
            index = 0
