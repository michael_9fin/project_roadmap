deps: ## Install dependencies
	pip install pip --upgrade
	pip install pip-tools
	pip-compile requirements.ini
	pip install -r requirements.txt

pdf: ## Create the PDF project roadmap
	python -m main

venv: ## Create a virtual environment
	python3 -m venv venv

lint: ## Lint directory
	python -m black .
	python -m flake8 --exclude venv
	python -m ruff .

